import sys

## entering large numbers is known to mess up the formatting, the triangle may stop looking like a triangle even though still technically a traingle.
try:
    height = int(sys.argv[1])
except:
    print("At least tell me how tall you want the triangle to be...")
    print("do it like this:")
    print("Python pt.py 10")
    print("10 is the height")
    
n = height - 1

## proceed to printing next row
for row in range(0, height):
    print("   "*row, end="")
    x = 1
    ## this loop prints a single row
    for k in range(0, n):
        print(int(x), end="     ")
        ## calculating binomial coefficients
        x = x * (n - k) / (k + 1)
    print("1")
    n = n - 1
print("Triangle may look weird if height is too large, but believe me it still is a triangle")
