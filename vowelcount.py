import sys
import re 

##a very good evening to you, or morning or whatever it is right now

def vowelCount(myString):
      vowels=0
      for i in myString:
            if(i=='a' or i=='e' or i=='i' or i=='o' or i=='u' or i=='A' or i=='E' or i=='I' or i=='O' or i=='U'):
                  vowels=vowels+1
      return vowels

##getting filename from commandline arguments
try:
      filename = str(sys.argv[1])
      file = open(filename, 'r')
except:
      print("Looks like there were problems opening the file")
      print("Make sure your command looks like this:")
      print("python vowelcount.py sample_vowelcount.txt")
      print("sample_vowelcount.txt is the filename (can be relative or absolute).\n\n\n")
      

##making a list of all words in the file by using regular expressions.
allWords = re.findall(r"\w+", file.read())

maxVowels = 0

##list of words having maximum vowels; because getting the output in a list may come in handy
selectedWords = []

for word in allWords:
      if vowelCount(word)==maxVowels:
            selectedWords.append(word)
            
      if vowelCount(word)>maxVowels:
            selectedWords = []
            selectedWords.append(word)
            maxVowels = vowelCount(word)
            
print("Maximum vowels encountered in a word were %d." % maxVowels)
print("Following are the words which had %d vowels." % maxVowels)
print(selectedWords)
