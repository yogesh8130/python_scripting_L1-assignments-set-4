import sys

# the function you asked for
def justify(string, width):
    string = string.split(" ")
    totalLength = 0
    for i in string:
        totalLength = totalLength + len(i)
    if (totalLength > width):
        raise Exception("\n\n\n\nThe width you specified is smaller than the longest string \
\nI can't fit a truck in a car park.\n\n\n")

    totalSpace = width - totalLength
    nspace = len(string) - 1

    for i in range(0, nspace):
        string.insert(((i*2)+1), totalSpace//nspace)

    spaceleft = totalSpace % nspace
    for i in range(0, spaceleft):
        string[((i*2)+1)] = string[((i*2)+1)] + 1
    return(string)

# print the justified string (that I made for ease)
def printjust(string):
    for i in range(0, len(string)):
        if(i%2 == 0):
            print(string[i], end="")
        else:
            print(" "*int(string[i]), end="")
    print("")

################################################################################################

try:
    filename = str(sys.argv[1])
    file = open(filename, 'r')
except:
    print("\n\nFILE ERROR:\nLooks like there were problems opening the file")
    print("Make sure your command looks like this:")
    print("python justify.py sample_justify.txt 20")
    print("sample_justify.txt is the filename (can be relative or absolute).\n\n\n")

try:
    width = int(sys.argv[2])
except:
    print("\n\nWIDTH ERROR:\nWidth parameter doesn't seem correct")
    print("Make sure your command looks like this:")
    print("python justify.py sample_justify.txt 20")
    print("20 is the width and it MUST be integer.\n\n\n")

line = file.readline()
line = line.rstrip()
while line:
    line = justify(line, width)
    printjust(line)
    line = file.readline()
    line = line.rstrip()

file.close()

