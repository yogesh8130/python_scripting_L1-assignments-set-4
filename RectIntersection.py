import sys

def intersection(coord):
    # r1 is rect 1 and r2 is rect 2
    # r1x1, r1y1, r1x2, r1y2, r2x1, r2y1, r2x2, r2y2
    #  00    01    10    11    20    21    30    31

    # checking if intersection exists
    if((coord[1][0] < coord[2][0]) or \
       (coord[0][0] > coord[3][0]) or \
       (coord[1][1] < coord[2][1]) or \
       (coord[0][1] > coord[3][1])):
        return("None")
    # finding coords of intersection in case it does exist
    else:
        xcoord = [coord[0][0], coord[1][0], coord[2][0], coord[3][0]]
        ycoord = [coord[0][1], coord[1][1], coord[2][1], coord[3][1]]
        xcoord.sort()
        ycoord.sort()
        # Just trust me, this might look like I'm hardcoding but sorting actually does the trick to find the intersection.
        # This is not hardcoding, you can check for other cases yourself.
        rect3 = [(xcoord[1], ycoord[1]), (xcoord[2], ycoord[2])]
        return(rect3)
    
##################################################################################################
    
## Test cases
print("Let's check for intersections between rectangles:")

print("\ncase 1: 100, 140, 300, 270, 200, 80, 350, 200")
coord = ((100, 140), (300, 270), (200, 80), (350, 200))
print("Intersection: ", intersection(coord))

print("\nCase 2: 80, 50, 170, 120, 100, 140, 300, 270")
coord = ((80, 50), (170, 120), (100, 140), (300, 270))
print("Intersection: ", intersection(coord))

print("\nCase 3: 80, 50, 170, 120, 200, 80, 350, 200")
coord = ((80, 50), (170, 120), (100, 140), (300, 270))
print("Intersection: ", intersection(coord))

## your case here... enter as commandline parameters:
print("\nNow we'll try with your case...")
try:
    coord = ((int(sys.argv[1]), int(sys.argv[2])), (int(sys.argv[3]), int(sys.argv[4])), \
         (int(sys.argv[5]), int(sys.argv[6])), (int(sys.argv[7]), int(sys.argv[8])))
    print("Intersection: ", intersection(coord))
except:
    print("\n\nOOPS!!! Are you sure your parameters were supplied correctly? Looks to me like they were not!!")
    print("Or maybe the rectangles exist in different dimensional realities....")
    print("\nDo it Like this:  python RectIntersection.py 80 50 170 120 100 140 300 270")
